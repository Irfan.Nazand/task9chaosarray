﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Task9ChaosArray
{
    class ChaosArray
    {
        public static int randomObject()
        {
            //Creating a Random object
            Random randomNumber = new Random();
            // Generate a random index less than the size of our array, which is 7.
            int randomPlaceOfNumber = randomNumber.Next(0, 6);
            return randomPlaceOfNumber;
        }

        // Add method that adds an item in the Random place of the array
        // If index position is already taken, message is posted onto console
        public static void InsertMethod<T> (T item, object[] myArray)
        {
            try
            {
                int randomPlaceOfNumber = randomObject();
                if (myArray[randomPlaceOfNumber] != null)
                {
                    Console.WriteLine("Alreday a value is inserted in this position, trying another position");
                }
                else
                {
                    myArray[randomPlaceOfNumber] = item;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        // Retrive method, retrives object from array. 
        // If landed on a position in array where there is no item found.
        // writes message to user and tries again until lands on correct value
        public static Object RetrieveMethod<T>(object[] myArray) 
        {
            int randomPlacOfNumber = randomObject();
            try
            {
                if (myArray[randomPlacOfNumber] == null) 
                {
                    Console.WriteLine("Landed on NULL, item not found, trying again");
                    return RetrieveMethod<object>(myArray);
                }
                else
                {
                    Console.WriteLine("");
                    return myArray[randomPlacOfNumber];
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return RetrieveMethod<object>(myArray);
            }
        }
    }
}
