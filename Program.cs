﻿using System;

namespace Task9ChaosArray
{
    class Program
    {
        static void Main(string[] args)
        {
            // define Array myArray
            object[] myArray = new object[7];

            // Add objects/values into myArray with the help of InsertMethod
            ChaosArray.InsertMethod<object>("Irfan", myArray);
            ChaosArray.InsertMethod<object>("Lewandowski", myArray);
            ChaosArray.InsertMethod<object>(1234, myArray);
            ChaosArray.InsertMethod<object>(5678, myArray);

            // Prints values to console
            Console.WriteLine(ChaosArray.RetrieveMethod<object>(myArray));
            Console.WriteLine("");
            Console.WriteLine(ChaosArray.RetrieveMethod<object>(myArray));
            Console.WriteLine("");
            Console.WriteLine(ChaosArray.RetrieveMethod<object>(myArray));
            Console.WriteLine("");
            Console.WriteLine(ChaosArray.RetrieveMethod<object>(myArray));
        }
    }
}
